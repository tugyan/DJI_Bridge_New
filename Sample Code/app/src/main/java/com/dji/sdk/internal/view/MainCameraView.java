package com.dji.sdk.internal.view;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.dji.sdk.R;

import com.dji.sdk.internal.controller.DJISampleApplication;

import com.dji.sdk.internal.model.ViewWrapper;
import com.dji.sdk.internal.utils.DialogUtils;
import com.dji.sdk.internal.utils.GeneralUtils;
import com.dji.sdk.internal.utils.ModuleVerificationUtil;

import dji.common.error.DJIError;
import dji.common.util.CommonCallbacks;
import dji.keysdk.BatteryKey;
import dji.keysdk.KeyManager;
import dji.keysdk.callback.GetCallback;
import dji.keysdk.callback.KeyListener;


/**
 * This view is in charge of showing all the demos in a list.
 */

public class MainCameraView extends FrameLayout {

    private AppCompatActivity MainAct;

    public MainCameraView(Context context) {
        this(context, null, 0);
    }

    public MainCameraView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainCameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        MainAct = (AppCompatActivity) context;
        initView(context);
    }

    private Button cmpntBtn;
    private Button takeOffBtn;
    private Button landBtn;
    private ProgressBar mBatteryBar;

    private void initView(Context context) {

        final LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_main_camera_view, this);

        cmpntBtn = (Button) findViewById(R.id.components_btn);
        takeOffBtn = (Button) findViewById(R.id.take_off_btn);
        landBtn = (Button) findViewById(R.id.land_btn);
        mBatteryBar = (ProgressBar) findViewById(R.id.battery_bar);
        ActionBar actionBar = MainAct.getSupportActionBar();
        actionBar.hide();

        cmpntBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GeneralUtils.isFastDoubleClick()) return;
                DJISampleApplication.getEventBus()
                        .post(new ViewWrapper(new DemoListView(getContext()),
                                R.string.activity_component_list));
            }
        });

        takeOffBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ModuleVerificationUtil.isFlightControllerAvailable()) {
                    DJISampleApplication.getAircraftInstance()
                            .getFlightController()
                            .startTakeoff(new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    DialogUtils.showDialogBasedOnError(getContext(), djiError);
                                }
                            });
                }

            }
        });

        landBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ModuleVerificationUtil.isFlightControllerAvailable()) {

                    DJISampleApplication.getAircraftInstance()
                            .getFlightController()
                            .startLanding(new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    DialogUtils.showDialogBasedOnError(getContext(), djiError);
                                }
                            });
                }
            }
        });

        BatteryKey batteryKey = BatteryKey.create(BatteryKey.CHARGE_REMAINING_IN_PERCENT);
        // Example of getting a value using Async interface
        KeyManager.getInstance().getValue(batteryKey, new GetCallback() {
            @Override
            public void onSuccess(final @NonNull Object o) {
                if (o instanceof Integer) {
                    mBatteryBar.setProgress(((Integer) o).intValue());
                }
            }

            @Override
            public void onFailure(@NonNull DJIError djiError) {
                DialogUtils.showDialogBasedOnError(getContext(), djiError);
            }
        });
        KeyListener batteryListener = new KeyListener() {
            @Override
            public void onValueChange(@Nullable Object o, @Nullable Object o1) {
                if (o1 instanceof Integer) {
                    mBatteryBar.setProgress(((Integer) o1).intValue());
                }
            }
        };
        KeyManager.getInstance().addListener(batteryKey, batteryListener);

        DJISampleApplication.getEventBus().register(this);
    }

    @Override
    protected void onAttachedToWindow() {

        //Remove notification bar
        MainAct.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        MainAct.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        MainAct.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActionBar actionBar = MainAct.getSupportActionBar();
        actionBar.show();
        super.onDetachedFromWindow();
    }

}
