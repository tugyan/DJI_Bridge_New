package com.dji.sdk.internal.utils;

/**
 * Created by bilal on 13.02.2017.
 */

public interface AsyncResponse {
    void processFinish(String output);
}
