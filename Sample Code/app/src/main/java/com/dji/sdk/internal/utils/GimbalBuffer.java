package com.dji.sdk.internal.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bilal on 23.04.2017.
 */

public class GimbalBuffer {
    public ArrayList<GimbalPacket> gimbalBuffer;
    Comparator c;
    public GimbalBuffer() {
        c = new Comparator<GimbalPacket>()
        {
            @Override
            public int compare(GimbalPacket t1, GimbalPacket t2) {
                if (t1.data[0] < t2.data[0]) {
                    return -1;
                }
                else if (t1.data[0] == t2.data[0]) {
                    return 0;
                }
                return 1;
            }
        };

        gimbalBuffer = new ArrayList<>();
    }

    public void add(double[] dt) {
        gimbalBuffer.add(new GimbalPacket(dt));
    }

    public double[] getClosestData(long ts) {
        if (gimbalBuffer.isEmpty()) return null;
        double[] dummy = {(double)ts, 0, 0, 0 };
        int idx = Collections.binarySearch(gimbalBuffer, new GimbalPacket(dummy), c);

        Log.e("IDX: ", Integer.toString(idx));
        if (idx < 0) {
            idx = -(idx + 1);
            if (idx == gimbalBuffer.size()) {
                    return gimbalBuffer.get(idx - 1).data;
            }
            else if (idx == 0) {
                if ((gimbalBuffer.get(0).data[0] - ts) < 300) {
                    return gimbalBuffer.get(0).data;
                }
                return null;
            }
        }
        return gimbalBuffer.get(idx).data;
    }

    public double[] getValue(int idx) {
        return gimbalBuffer.get(idx).data;
    }

    class GimbalPacket {
        double[] data;
        GimbalPacket(double[] dt) {
            data = dt;
        }
    }

}
