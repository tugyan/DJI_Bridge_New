package com.dji.sdk.internal.view;

import android.content.Context;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.dji.sdk.R;
import com.dji.sdk.internal.controller.DJISampleApplication;
import com.dji.sdk.internal.model.ViewWrapper;
import com.dji.sdk.internal.utils.AsyncResponse;
import com.dji.sdk.internal.utils.GeneralUtils;
import com.dji.sdk.internal.utils.GimbalBuffer;
import com.dji.sdk.internal.utils.ModuleVerificationUtil;
import com.dji.sdk.internal.utils.ToastUtils;
import com.squareup.otto.Subscribe;

import dji.common.flightcontroller.FlightControllerState;
import dji.common.gimbal.GimbalState;
import dji.sdk.base.BaseProduct;
import dji.sdk.products.Aircraft;
import com.dji.sdk.internal.utils.TCP_Client;

import java.nio.ByteBuffer;


/**
 * Created by dji on 15/12/18.
 */
public class MainContent extends RelativeLayout implements AsyncResponse, TCP_Client.OnMessageReceived {

    public static final String TAG = MainContent.class.getName();

    public MainContent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private TextView mTextConnectionStatus;
    private TextView mTextProduct;
    private TextView mTextModelAvailable;
    private Button mBtnOpen;
    private Button mCnctBtn;
    private EditText mIpText;
    protected boolean isIMUSaving = true;

    private connectTask conctTaskGimball = null;
    static TCP_Client mTcpConn;
    private String hostIP;
    private boolean isTCPConnected = false;

    private BaseProduct mProduct;

    private GimbalBuffer gbBuf = null;


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        DJISampleApplication.getEventBus().register(this);
        initUI();
    }

    private void initUI() {
        Log.v(TAG, "initUI");
        gbBuf = new GimbalBuffer();

        mTextConnectionStatus = (TextView) findViewById(R.id.text_connection_status);
        mTextModelAvailable = (TextView) findViewById(R.id.text_model_available);
        mTextProduct = (TextView) findViewById(R.id.text_product_info);
        mBtnOpen = (Button) findViewById(R.id.btn_open);
        mCnctBtn = (Button) findViewById(R.id.cnct_btn);
        mIpText = (EditText) findViewById(R.id.ip_addr_field);


        mBtnOpen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GeneralUtils.isFastDoubleClick()) return;
                DJISampleApplication.getEventBus()
                        .post(new ViewWrapper(new MainCameraView(getContext()),
                                R.string.activity_main_camera));
            }
        });

        mCnctBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hostIP = mIpText.getText().toString();
                if (!isTCPConnected) {
                    conctTaskGimball = new connectTask();
                    conctTaskGimball.delegate = (MainContent) v.getParent();
                    conctTaskGimball.msgListener = (MainContent) v.getParent();
                    conctTaskGimball.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    mCnctBtn.setText("Disconnect");
                    isTCPConnected = true;
                }
                else {
                    mTcpConn.stopClient();
                    mCnctBtn.setText("Connect");
                    isTCPConnected = false;
                }
            }
        });

    }

    @Override
    protected void onAttachedToWindow() {
        Log.d(TAG, "Comes into the onAttachedToWindow");
        refreshSDKRelativeUI();
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private void updateVersion() {
        String version = null;
        if (mProduct != null) {
            version = mProduct.getFirmwarePackageVersion();
        }

        if (version == null) {
            mTextModelAvailable.setText("N/A"); //Firmware version:
        } else {
            mTextModelAvailable.setText(version); //"Firmware version: " +
        }
    }

    @Subscribe
    public void onConnectivityChange(DJISampleApplication.ConnectivityChangeEvent event) {
        post(new Runnable() {
            @Override
            public void run() {
                refreshSDKRelativeUI();
            }
        });
    }

    private void refreshSDKRelativeUI() {
        mProduct = DJISampleApplication.getProductInstance();
        Log.d(TAG, "mProduct: " + (mProduct == null ? "null" : "unnull"));
        if (null != mProduct && mProduct.isConnected()) {
            mBtnOpen.setEnabled(true);

            String str = mProduct instanceof Aircraft ? "DJIAircraft" : "DJIHandHeld";
            mTextConnectionStatus.setText("Status: " + str + " connected");
            updateVersion();

            if (null != mProduct.getModel()) {
                mTextProduct.setText("" + mProduct.getModel().getDisplayName());

                // Register Gimball Callback for sending data to PC.
                if (ModuleVerificationUtil.isGimbalModuleAvailable()) {
                    DJISampleApplication.getProductInstance().getGimbal().setStateCallback(new GimbalState.Callback() {
                        @Override
                        public void onUpdate(@NonNull GimbalState gimbalState) {
                            double[] data = {(double)(System.currentTimeMillis()),
                                    gimbalState.getAttitudeInDegrees().getYaw(),
                                    gimbalState.getAttitudeInDegrees().getPitch(),
                                    gimbalState.getAttitudeInDegrees().getRoll()};
                            gbBuf.add(data);
                        }
                    });
                }
                // Register IMU Callback for sending data to PC.
                if (ModuleVerificationUtil.isFlightControllerAvailable()) {
                    DJISampleApplication.getAircraftInstance().getFlightController().setStateCallback(
                        new FlightControllerState.Callback() {
                            @Override
                            public void onUpdate(@NonNull FlightControllerState imuState) {
                                if (mTcpConn != null && isIMUSaving) {
                                    String str2send = "i" + (System.currentTimeMillis() - mTcpConn.getConnectionTime()) +
                                            " " + imuState.getAircraftLocation().getLatitude() +
                                            " " + imuState.getAircraftLocation().getLongitude();
                                    str2send += " " + imuState.getAircraftLocation().getAltitude();
//                                    if (imuState.isUltrasonicBeingUsed()) {
//                                        str2send += " " + imuState.getUltrasonicHeightInMeters();
//                                    }
//                                    else {
//                                        str2send += " " + imuState.getAircraftLocation().getAltitude();
//                                    }

                                    str2send += " " + imuState.getVelocityX() +
                                            " " + imuState.getVelocityY() +
                                            " " + imuState.getVelocityZ();

                                    mTcpConn.sendMessage(str2send);
                                }
                            }
                        }
                    );
                }
            }
            else {
                mTextProduct.setText(R.string.product_information);
            }
        } else {
            mBtnOpen.setEnabled(false);

            mTextProduct.setText(R.string.product_information);
            mTextConnectionStatus.setText(R.string.connection_loose);
        }
    }

    public static TCP_Client getTCPClient() {
        return mTcpConn;
    }

    @Override
    public void messageReceived(String message) {
        try {

            if (message != null) {
                if(message.charAt(0) == 'g') {
                    String number = message.substring(1);
                    byte[] bytesa = number.getBytes();
                    ByteBuffer tsBytes = ByteBuffer.wrap(bytesa);
                    int ts = tsBytes.getInt();
                    long connTime = mTcpConn.getConnectionTime();
                    double[] data = gbBuf.getClosestData(ts + connTime);
                    if (data != null) {
                        ToastUtils.setResultToToast("Sent time: " + Long.toString((long)data[0] - connTime));
                        String str2send = "g" + Long.toString((long)data[0] - connTime) + " " + (float)data[1] + " " + (float)data[2] + " " + (float)data[3];
                        mTcpConn.sendMessage(str2send);
                    }
                    else {
                        ToastUtils.setResultToToast("NA: " + Double.toString(gbBuf.getValue(0)[0]));
                        mTcpConn.sendMessage("gNaN");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class connectTask extends AsyncTask<String, String, String> {
        public AsyncResponse delegate = null;
        public TCP_Client.OnMessageReceived msgListener = null;
        @Override
        protected String doInBackground(String... message) {

            //we create a TCPClient object and
            mTcpConn = new TCP_Client(msgListener, hostIP , 5275);
            String result = mTcpConn.run();
            return result;
        }


        @Override
        protected void onPostExecute(String result) {
            Log.e("result", result);
            delegate.processFinish(result);
        }
    }

    @Override
    public void processFinish(String output) {
        ToastUtils.setResultToToast(output);
        mCnctBtn.setText("Connect");
        isTCPConnected = false;
    }

}
