package com.dji.sdk.internal.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author Prashant Adesara
 * Handle the TCPClient with Socket Server.
 * */

public class TCP_Client {

    private String serverMessage;
    /**
     * Specify the Server Ip Address here. Whereas our Socket Server is started.
     * */
    private static String SERVER_IP = "127.0.0.1"; // your computer IP address
    //public static final String SERVER_IP = "10.42.0.1";
    public static int serverPort;

    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;

    private PrintWriter outBuffer = null;
    private BufferedReader inBuffer = null;
    private Socket socket = null;
    private long connTime = System.currentTimeMillis();

    /**
     *  Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCP_Client(final OnMessageReceived listener, String ip ,int port)
    {
        mMessageListener = listener;
        serverPort = port;
        SERVER_IP = ip;
    }

    /**
     * Sends the message entered by client to the server
     * @param message text entered by client
     */
    public void sendMessage(String message){
        if (outBuffer != null && !outBuffer.checkError()) {
            //System.out.println("message: "+ message);
            outBuffer.println(message);
            outBuffer.flush();
        }
    }

    public void stopClient(){
        mRun = false;

        if (socket != null) {
            try {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            } catch (IOException exc) {
                Log.e("SocketError", "Socket could not be closed.");
            }
        }
    }

    public long getConnectionTime() {
        return connTime;
    }

    public String run() {

        mRun = true;

        try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            //create a socket to make the connection with the server
            socket = new Socket(serverAddr, serverPort);
            connTime = System.currentTimeMillis();
            try {
                socket.setTcpNoDelay(true);

                //send the message to the server
                outBuffer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                // Send connection time
                this.sendMessage("b" + Long.toString(connTime));

                //receive the message which the server sends
                inBuffer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    serverMessage = inBuffer.readLine();
                    Log.e("MESSAGE RCV: ",serverMessage);

                    if (serverMessage != null && mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(serverMessage);
                        Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + serverMessage + "'");
                    }
                    serverMessage = null;
                }
            }
            catch (Exception e)
            {
                Log.e("TCP SI Error", "SI: Error", e);
                e.printStackTrace();
            }

            return "Disconnected";

        } catch (Exception e) {

            Log.e("TCP SI Error", "SI: Error", e);
            return "Connection failed.";
        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    public interface OnMessageReceived {
        void messageReceived(String message);
    }


}